{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/release-22.11";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
  flake-utils.lib.eachDefaultSystem (system:
  with import nixpkgs { inherit system; };
  rec {
    packages = rec {
      default = citations-cli;
      citations-cli = with python3Packages; buildPythonApplication {
        pname = "citate";
        version = "SNAPSHOT";
        src = ./.;
        propagatedBuildInputs = [
          pillow
          setuptools
        ];
      };
    };

    apps = rec {
      default = citations-cli;
      citations-cli = flake-utils.lib.mkApp { drv = packages.citations-cli; };
    };
  });
}
